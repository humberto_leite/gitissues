# Gitissues

This project is a test for the company IRONTEC, it lists all the issues from github with a pagination.

It was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0.


# Usage

Run `npm install`
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

# Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

# View Customization
This project uses [Material Design](https://github.com/angular/material2)
