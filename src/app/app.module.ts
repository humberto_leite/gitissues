import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent,SettingsDialog } from './app.component';

import { ListIssuesComponent } from './list-issues/list-issues.component';
import {MaterialModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {IssusService} from './list-issues/list-issues.service';

@NgModule({
  declarations: [
    AppComponent,
    ListIssuesComponent,
    SettingsDialog
  ],
  entryComponents: [
    AppComponent,
    SettingsDialog
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    MaterialModule.forRoot()
  ],
  providers: [IssusService],
  bootstrap: [AppComponent]
})
export class AppModule { }
