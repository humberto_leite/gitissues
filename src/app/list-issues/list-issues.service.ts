import {Injectable} from "@angular/core";
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
@Injectable()
export class IssusService {
  user: string;
  repo:string;
  url:string;
  constructor(private http: Http) { }

  getProject(url:string){
    this.parseUrl(url);
    return  this.http.get("https://api.github.com/repos/"+this.user+"/"+this.repo)
               .map(res => res.json());
  }
  getIssues(page:number){
    return  this.http.get("https://api.github.com/repos/"+this.user+"/"+this.repo+"/issues?filter=all&page="+page)
               .map(res => res.json());
  }
  
  private parseUrl(url:string){
    this.url = url;
    let urlArray = url.split('/');
    this.user = urlArray[3];
    this.repo = urlArray[4];

  }


}
