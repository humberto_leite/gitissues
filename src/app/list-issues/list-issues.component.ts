import { Component, ViewChild, ViewContainerRef,OnInit } from '@angular/core';
import {MdSidenav, MdDialog, MdDialogConfig} from "@angular/material";
import { FormsModule } from '@angular/forms';

import {IssusService} from "./list-issues.service";



@Component({
  selector: 'app-list-issues',
  templateUrl: './list-issues.component.html',
  styleUrls: ['./list-issues.component.css']
})
export class ListIssuesComponent implements OnInit {
  issues:string;
  project:any;
  url:string;
  total:number;
  totalPage:number;
  currentPage:number;
  constructor(public issusService: IssusService) {
    // initial project
    this.url = "https://github.com/irontec/sngrep";

    // Reference to this collection's model.
    this.issusService = issusService;


  }

  ngOnInit(){
    // Load initial project set in "this.url"
    this.getProject();
  }

  @ViewChild('sidenav') sidenav: MdSidenav;
  currentIssue = {};


  getProject() {
    this.issusService.getProject(this.url)
    .subscribe(
      data => {
        this.project = data;

        // Total de issues open
        this.total = this.project.open_issues_count

        // Calculate how many pages
        this.totalPage = Math.ceil(this.total / 30)
      },
      error=>alert(error),
      () => this.paginate(1)
    );
  }

  /*
  * Get the previous page
  */
  private paginate(page:number) {
    this.currentPage = page;

    // Get issues
    this.issusService.getIssues(page).subscribe(
      data => this.issues = data,
      error=>alert(error),
      function(){

      }
    )
  }

  /*
  * Get the previous page
  */
  previous() {
    this.currentPage--;
    this.paginate(this.currentPage);
  }

  /*
  * Get the next page
  */
  next() {
    this.currentPage++;
    this.paginate(this.currentPage);
  }

  /**
  * Show issue in sidenav(right)
  */
  showIssue(issue) {
    this.currentIssue = issue;
    this.sidenav.open();
  }


}
