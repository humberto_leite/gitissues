import { Component, ViewChild, ViewContainerRef,OnInit } from '@angular/core';
import {MdSidenav, MdDialog, MdDialogConfig} from "@angular/material";

/*
* Component Dialog for test ( Just to add more stuff in the code ) ;)
*/
@Component({
  selector: 'settings-dialog',
  template: `
  <label>Solo una prueba</label>

  `
})

/*
* Settings for Dialog
*/
export class SettingsDialog {

}

/*
* App Component
* our top level component that holds all of our components
*/
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit {
  ngOnInit(){
  }

  // Reference to this MdDialog and ViewContainerRef.
  constructor(public dialog: MdDialog, public vcr: ViewContainerRef) {
  }

  // Methos from open dialog
  openDialog() {
    const config = new MdDialogConfig();
    config.viewContainerRef = this.vcr;
    this.dialog.open(SettingsDialog, config);
  }
}
