import { GitissuesPage } from './app.po';

describe('gitissues App', () => {
  let page: GitissuesPage;

  beforeEach(() => {
    page = new GitissuesPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
